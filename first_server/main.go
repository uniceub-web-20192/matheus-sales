package main

import (
	"io/ioutil"
	"net/http"
	"time"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
		Addr:        "192.168.0.22:8082",
		IdleTimeout: duration,
		//	Handler    :
	}

	http.HandleFunc("/cebola", cebola)

	server.ListenAndServe()
}

func cebola(w http.ResponseWriter, r *http.Request) {
	bs, _ := ioutil.ReadFile("cebola.jpeg")
	w.Write([]byte(bs))

}
